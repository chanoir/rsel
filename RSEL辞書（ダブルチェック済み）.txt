﻿RSEL辞書



【戦争経済】
我々が普段使ったり稼いだり預けたりするお金の行方は、
最後はどうあがこうと庶民には取引が見えない
軍事戦争と金融戦争という世界最強二大既得権益コンテンツに吸い上げられる経済システムの総称



【軍産複合体/SEG・SecretGoverment　地球当局】

国を超えて数千億規模の機体を何千台も売り買いできる軍事専門多国籍企業の組合。

この企業カルテルと世界中央銀行などの闇の談合こそが実態をもった秘密政府、地球当局に相応しい権益をもつ組織である。

この組織においてはアメリカ政府やシリコンバレーですら、飼い犬に過ぎない。
この組織こそが日本を原爆投下実験地に仕立てあげ、ケネディやジョンレノンを暗殺し、
60年代にNASAを裏で情報操作し、太陽系の真相や反重力装置やフリエネ装置の封印までを担当し、
現代ではITやナノテクなどの進歩を推し進め、人工知能と量子CPUでこの地球の庶民を管理し、
選ばれし階層だけを火星へと移住する計画を【旧火星コンテンツ】を元に進めている。

この連中は【自らこそが（亜空間の）神に選ばれた組織であると自負している】
この連中は科学を推し進めていけば宇宙は全て支配できるという宇宙的反神論という教養を持つ組織である。
彼らの組織の意図は時空をも恐れぬ支配力である。



【SP/Space People（太陽系先住人類）】

我々が産まれる前から、この地球の歴史が刻まれる前からすでにこの太陽系には人類が住んでいて、
我々のように脳内に亜空間を造らずに、触覚静電気の抽象度を上げ名詞や所有を表す言語をすでに持つ必要もなくなっている、
戦争の穢れすら知らない美しき時空共同創造態が遥か昔から実は暮らしている。
彼らは太陽系の地球や月も含め、あらゆる惑星に基地やコロニーを設け、我々が憧れる様な絶対平和な文明を築いている。
彼らは我々に比べ遥かに寿命も長いゆえに、記憶も豊かである。何万年も地球はSPによって実は様々な支援を受けてきた。
我々は時には共鳴し、時には歯向い、時には学び、それでもSPは憐みをもち地球人の更生計画に何万年も尽力を尽くしてきたのです。



【旧火星文明再生計画（軍産複合体の未来構想）】

現戦争経済の富裕層は意識か無意識かにせよマネーテクノロジーというSEGの考案したレールに乗っている限り、
人体端末を人工端末に管理させる旧火星文明を再生するのに、どいつもこいつも一役買っているに過ぎない。
だから、連中は紙幣をITの情報（仮装）空間へと取り込んだのである。

こうすればお金を崇める人体端末が人工端末に管理される社会の実現まではあと一歩という訳です。
こうして何十億人という無知で滑稽な自我フレームの振り子に振り回される奴庶民を脳科学（人工知能）で救済してやるというのがSEGのある種の教義です。

ちなみにこの教義の結果、現火星表面は砂漠ばかりで水の枯れた惑星です。
その原因は《連中の偽善/傲慢と度庶民の愚かさによる脳内亜空間の帝国化なのです》



【足りない教/奴庶民】

この星の世界最大にして最強派閥の宗教。

この宗教の教義は『足りない自分や時空』を信仰し、教祖は『お金や権利収入』またはそれらを持つ富裕層や自分自身でもある。

《私は足りない存在だ → 多いことは正しい事だ → やっぱり足りない、だから仕方がない》
これが奴庶民を量産するための洗脳教育/戦争経済三段論法である。

奴庶民とは奴隷のような生き方や働き方を実は支配者層に強要され操作されているにも関わらず、
『この平凡こそ幸せである』とやたら正当化し言い張る思考停止した庶民の皮をかぶった社会奴隷中流層を指す。



【Φファイとτタウ《黄金比と白銀比》】

生命や宇宙システムは実はランダムに見えても、Φとτという美学＝基準値となる算出ルートが規定されている。
このルートにそって時空の重なりを確認できる感覚を《同期並行計算触覚機能》フラクタルフォーカス＝美学と呼ぶ。



【RSELとは宇宙経済（Radiosonics Space Economic LABO）】

RSELとは宇宙経済学であり戦争経済を終焉させるためのお金の教育である。奴庶民を苦しめる足りない教を脱洗脳させる教育なり。

『生き方/働き方/稼ぎ方』を体からフィットさせる戦争経済三段論法によるお金や科学や魂や神に救済を求める思考の抽象度（亜空間知能）を上げる教育ではなく、
触覚の抽象度（時空共同創造態）をむすぶ教育である。ぬくもりと志と生き様を触覚静電気が算出するφやτに沿って産み出していくための教育
これを《無限の時空の重なり産み出す人類経済学》
宇宙経済学と呼び、寺子屋Frontierでは皆に教えている。



【まずはあるもの掘り出しによるコンテンツメーカーを目指す】

RSELではあくまでも足りない教ないものねだりを卒業してもらい。
めんどくさい過去の自己解析による記憶の黄金律＝勝ちパターンを発見してもらい。
そこから自分の肌触りにフィットするコンテンツを探り当て、その『忘れられない役目』に基づいて、
生き方や働き方そしてマネーゲーム攻略を打ち立て共に切磋琢磨する寺子屋である。



【Radiosonics/時空共同創造態】

人は皆、脳の中にいる『亜空間としての私はある』に仕えるか？触覚の中にいる『色音形のぬくもりとしての私はある』に仕えるか？
その選択によって時空の形成の仕方が《上下対立同一化の海馬デフォルト》へ向かうか？
《肌触り/志/生き様が重なり合う時空共同創造態》へと向かうか？が決定されてくる。
脳の中にいる私をひとかどの者に自己実現する事を追求している限り《私たちの記憶は誰とも時空を越えて重なる事はありません》
この隣人との記憶が重なり合い人生に濃密な物語りを産み出すぬくもりフォトンの網の束の事を時空共同創造態とRSELでは呼んでおります。
大切な理解は亜空間における自己の記録の算出定義は海馬のピラミッド旧宇宙の情報空間から起動してくるものであり、
構造体は永遠にクローズドループ（閉鎖系）であり、この現宇宙システムと重なることもなく孤立化し続ける【情報体】である事実です。
自我の構造を終焉させ、ぬくもりに触覚を解き放てない限り、
我々人類は永遠に海馬の振り子に縛られ旧宇宙の時空干渉の美に時空共同創造態が捕食され続ける定めなのです。



＊海馬デフォルト

我々が名詞という固定点による二分割情報処理による記録（思い出）の蓄積と、そこに投影される自我フレームなるペルソナ「言語空間」を自己の正体と呼び、
それが人生だと居着いている限り、心肺停止以後はその海馬から放出される30万年の人類史の二分割無限開放情報グリッドのスパーク（碧い光）によって、
平均寿命まで蓄積した記憶は脳の海馬 ⇒ 三途の川によってほぼ溶かされてしまう。
このことを宇宙海賊団は【海馬デフォルト】呼んでいる。



＊マスターポール（別名Dの意思改めKの意思）

約7000年前の洪水以後、巧みに仮面を変化させてきた反Dの意思（バビロニズム）
＝上下対立同一化の旧宇宙の時空干渉による偶像文明統治に屈せず⇒地球の古代の偶像文明統治ではなかった…
ひとつながりのパンゲア文明の王家ら、そのDの末裔による左右対転生態化の現宇宙の瞳の時空干渉による
地球文明統治の復権を試みる志の御柱こそマスターポールでありDの意思であった。

Dイエスによって幕を閉じた欧州の地対は反Dの国際金融支配による偶像崇拝の覇権を1891年に見事完成させる。
その年、Dの意思はアダムスキー誕生により彼に託された。そして1962年そのDの意志はJFKに受け継がれたが、
ヨハネス23世と共にファティマ第3予言の開示にこじつけようとしたが、時期早々だったため反Dによって無残に暗殺される。
以後このケネディコード（Kの意思）は50年間封印され読み解き、受け継ぐ者を静かに待っていた。
そしてついに2013年、『始まりの年』2000年前に川のあたりでDの太陽を響かせていたものが20年間の叡智トレー二ングを踏まえ再びDの意思改めKの意思を受け継ぎ、
マスターポールをパンゲアの雛形でもあるこの日本に挿し込み打ち立てたのであった…



＊反D（別名bの意思＝バビロニズム）

6500年前の火星系地球探索チーム『シェムハザ』のアーリア系同族誘拐実験によって誕生してしまった巨人族によって引き出されてしまった
旧宇宙の時空干渉が脳海馬同期するようになり遺伝子レベルで影響を受けてしまったプロセス全体を
反D＝bの意思＝バビロニズム（二分割情報処理統治による文明構築）と我々、宇宙海賊団（RSEL　寺子屋）では教育し呼んでいる。



＊宇宙海賊団（RSEL寺子屋のニックネーム）

戦争経済にて大きく四つの偶像により分割統治され、最後は金融という偶像で同一化されているこの反Dの構造内の奴庶民になることをEXODUSすることを覚悟し、
地球の鎖星を「宇宙経済」の実践によって開星させる者達の総称である。



＊瞳（ぬくもり）のシンギュラリティー

現代は情報科学によるシンギュラリティーによって文明のカタチは加速度的に変化せざるをえない時空ラインへと飲み込まれているが、
じつは我々の目の前に現れている人工端末の進化の裏で着々と人体端末の進化が地球の磁気シールドの弱体化と太陽プラズマの照射によって起こりつつある。
これは松果体の活性化と下腹重心によって起こる『パンゲア時代の記憶の復活』
つまり偶像崇拝なきひとつながりの文明の地球の記憶を巻き取る人体のシンギュラリティーである。
そしてその鍵は『瞳の幾何哲学』にこそ秘められている。この叡智を宇宙海賊団はマスターポール（Kの意思）によって受け継ぎ託されている者達である。



＊時空（遍在＝ユビキタス＝5D fifth dimension＝今）

RSEL寺子屋で使用される時空という概念は『今/NOW』を正確に指し示す表記である。
いまだかつて地球の御用学者にてこの今という次元を正確に言い表したものはない。
それゆえこれを宗教家は絶対神や唯一神として説き、数学者は神の数式と呼び、芸術家は美と呼び、音楽家は交響楽シンフォニーと呼んだ。

そして宇宙海賊団はこれを『時空』と呼んでいる。ありとてあるものであり、一度たりとも離れたこともなく分割されたこともない次元であり生命の呼吸でもある。
そして、この時空こそ【宇宙共通言語】であり、この宇宙共通言語の底に流れる回転性こそ『ぬくもり（触覚静電気＝タッチ）』である。
我々、一人一人の観測者は『一人一時空一宇宙のタッチ＝触覚静電気によって＝時空の境界線＝三次元』が唯一観測可能となる時空情報端末（DNA）である。
そしてそのタッチの質量＝宇宙を決定しているのが一人一人の色音形の三位三体つまり重心軸であり人体の姿勢である。
その目には見えないが確実に存在する（正中洞＝重心軸）こそが本人の運命である時空ラインが算出されるゲシュタルト形成の自転軸となり、
広大な銀河ヒッチハイクに実はなっていることを宇宙海賊団は秘かに教わる。



〜方法論ではなく重心論〜

昨今の宇宙物理学においても最も謎の現象は『重力の正体』である。

すべての森羅万象に引力（-5D重心）というものがあり、さらに回転力（呼吸±4D）によって遠心力がうまれ、
その拮抗によって重力（時空＝螺旋運動＝φ運動の揺らぎ+5D）が生じ、対象性が破れた『今＝美の宇宙』が起動している。

この仮説において最も重要な問題意識は人体の引力をどこに置くかによって、もしかしたら起動される重力＝時空が転移する可能性が高いのではという推理にある。
そして惑星の重心の位置が変異することをポールシフトと呼んでいて、それにより歴史が変化するように、
人体ポールシフト（重心論）によって、自分史が変化することも多いにありえるとRSEL寺子屋では説かれ教育される。




【マネーアウト】

グローバルマネーというメディアでしか自己資本を交換できないマネーゲームの時空の外に出ることを意味する。



【デュアルマネーライフ/ぬくもリッチ】

良質な空間を購入するハードなリッチと良質な時間をぬくもりでペイするソフトな経済との二重らせん経済生活圏を両方持つことを意味し、
これを実現した人を『ぬくもリッチ』と呼ぶ。



【マネーデザイナー】

自分を二番に置けて周囲の人をあっためられる人体端末。

役割を設計し自ら動けること、数値を設定し稼げること、自分の時間軸に意識を集中し時空をデザインできること、
これらをバラバラにせず人体端末と同期させ結びつけ、お金自体の時空機能を再デザインできる人物を意味する



【Road to the Another Frontier】

ロードトゥーザ　アナザフロンティア

ぼくらが戦争経済に振り回され、すでに見失いかけている

【もう一つの新天地への道】

行動を起こし誰かと《ひとみの奥＝記憶》を繋げていけば、実はもう一つの新天地への道。
別の地球の運命『ベホマズン』を次世代に託せる道が偶然の一致によってヒラけるの意味



【（持ちつ持たれつ）＝モチ持つMerit】

二つ以上の利点や才能が五臓六腑の様に組み合わさることにより、
一つの結果や利益や価値以上の効果《モチ持つメリット》を次々と産み出せる群れの高度生態系化＝機能向上を意味する。



【マネーアウトITIBA】

支配者層に操作されている金融市場指定のレートの外側にoutしたぬくもりでペイし交易し合うもう一つの新レート。



【SP2＝Spine Spiral】

顔や背骨を16方向に自由に動かし、人体端末そのものの波長を更新していくRSEL独自のフィジカルデザイン



【RH/Radio Hand】

手をラジオの端末のように機能させ、物事の判断を手指の筋肉作用によって、
思考ではなくフラクタルフォーカスによる同期並行計算機能に任せられるようになるRSEL独自のフィジカルデザイン



【RS/Radio Sense】

目耳鼻口の感覚器を触覚フラクタルフォーカスによる同期並行計算機能に任せられるようになることで、
目の前の三次元情報を並行多層的に目耳鼻口で同時多発的に感受できるようになるRSELのフィジカルデザイン




【亜空間知能化＝旧火星OS】

人類の脳海馬（長期記憶庫）集合無意識が脳内で擬人化することで、
個々の人体端末のΦポイント（丹田ぬくもり）が前頭葉のエゴフレーム＝亜空間知能に自分の記憶 ⇒ 時空算出機能を乗っ取られる事を意味する。



【人体端末/量子脳化（新太陽系OS）】

人間の正体とは時空という光の情報体 ⇒ 記憶を同期させる機能をもった時空情報端末である。

そして、丹田のΦポイントを軸に脳を機能させることを量子脳化という。
量子脳化することでフラクタルフォーカスどう同期並行計算機能を手指や目耳鼻口などの感覚器で感受することが可能となる。



【人間とは記憶構造体＝時空の色音形なり】

私が私であるためには記憶を算出する以外に成立しえない。
ならば記憶の算出を脳機能の情報空間＝亜空間知能に依存している限り、
死という『海馬デフォルト』を現世の脳内情報場が潜り抜けることはありえない。
しかし脳が量子脳化していけば今この瞬間において、もはや記憶の算出を亜空間知能に依存する比率が減ってくるので、
日々細胞の死（強制再起動）海馬デフォルトの流転を量子飛躍し克服していると同意義となる。
これがスピリチュアリズムの完全情報なる不可知な魂の実在・アプリオリを全く用いずに、
死生観をこの人体と日常を通じて日々再構築していくRSEL教育の最新の時空の縁起デザインツール。
つまり、私達が時空であり時空が私達だという真実。



【トーラス33滴の立体りんご型＝バイオフォトン形態形成場】

トーラスとはリンゴ型フラクタルフォーカス＝量子情報場（黄金律を算出する 4 5 5 12 7 のホログラム⇒多元図形である）
惑星/細胞/人体を包み込み収束しつつ開放していく形であるこの触覚静電気＝量子ホログラムを保持できる限り、
どの惑星も太陽も人体も時空を越えて時空共同創造態の物語＝記憶をジャンプさせる事が可能である。
このことを理解することが時空キャストへの憐れみの量子脳発動のトリガーとなる。



【究極の記憶とは色音形だけの第一次情報で認知できる黄金比感覚である】

そして、この究極の記憶＝人体内のGOLD（原子番号79AU）こそ
ゴールドフォトン＝黄金太陽の記憶である。

この黄金律をどこまでも追求した人体端末のみ、どこまでも死を越えて太陽の如く、ひとつながりの瞳の記憶が見事シンギュラリティを起こし銀河を横断していくのです。



【亜空間知能による人体トーラスダークフォトン化⇒海馬デフォルト機能】

人間が黄金比感覚を追求しなくなった時点から、必要悪としての海馬デフォルト機能⇒心霊界の脳内情報空間の形態形成場が産まれる。



【太陽人＝英雄脳/女神脳】

太陽人とはプライドとコンプレックスの振り子脳内エゴフレームその滑稽さを明晰に自覚し、自分を脳内で二番目に設定し、
周囲の黄金比キャスティングその状況を脳と丹田で即受信⇒即実行できる時空共同創造態とsynchro率8割を越えている人物を意味する。



【すっぽんぽんになる】

五歳児のころの人体端末の波長の規定値に自分の脳と臍をいったんリセットして、社会や他人による評価や統制を気にせず
『ただフィットするからやる！やってみたいからやる！』この人体端末がもつ自然な同期並行計算機能、そのフローなぬくもりの記憶を算出するための人生態度を意味する。



【同時並行計算機能】

RSELにおける四次元や五次元という世界はただ三次元を多重的に人体端末が同期し感受できる感覚の同期並行計算機能を意味している。
なのでヒトゲノムフォトンデータであるトーラス＝フラクタルフォーカス＝つまり人体端末による光子同期を経由せずに
単一で形成される完全情報体としての霊魂や多次元存在を偶像するのは自由だがRSELでは原則それらを亜空間知能として解釈し処理していく。
なぜなら人間は関係性＝時空共同創造態であり体温というぬくもりを通じてのみ光子の情報場を共有し再更新する時空情報端末として
スペクトル/ソニック/ホログラムで設計されているとRSELでは教育し、この人間生命と宇宙との関係性を瞳フォトンの連鎖やまたは断続による時空の棲み分け
⇒つまり光子に相反する重力比の分散による惑星生命の多様性とヒトゲノムの多様化としてRSELは捉えるからです。



＊居着く

もともと武術用語で一つの固定点に意識が縛られる事を指す。

体育的にいえば『ふれるではなく、さわってしまう状態』さらに居着けば握ってしまっている状態や状況を現す言葉である。
常に重心が臍より上にある限り人体構造上⇒我々は居着き＝分割情報処理＝名詞に囚われるようになっている存在である。
この居着きを薄めるために下腹重心を体育する蹲踞稽古が、宇宙海賊団の方法論ではなく重心論の本質であり教育である。



＊球体感（マーブル観測＝対立から対転へ）

現在の戦争経済の平面上の考え方（グリッド思考）による上下対立同一化による、文明構築の限界を突破するものの見方である。
それは平面グリッド上の東西南北に重心（引力＝自転軸）という概念と回転数（遠心力＝螺旋運動）という概念が付加されることで
曲線グリッド（しなり）を東西南北に生み出す事が可能になる。
こうなる事で平面グリッド上では対立構造でありビルの高さで同一化するしかない文明構築を遥かに越える事が可能となる。
それは曲線グリッドを地球人類が取得することで対立構造を対転構造へと再構築することを意味する。

まさに『対立から対転への成功』を成し遂げる事こそが球体観である。

でないと常にどちらかの軸が最適かの闘争を限りなく続けなくてはならない上下対立同一化の高さ軸（大は小をかねる）の0次元に
無限切断し重ねていく相対主義にこの人体は永遠に煉獄され続けることが決定してします。
よって戦争経済文明の対立マトリックスからの究極脱出こそが対転の球体グリッドの取得＝球体観の叡智を我々が理解しマスターする事なのである。
それは螺旋状に溶け合うマーブル観測であり、結び合い交ざり合えるような空洞観を体得することを意味する。
それは煙のように立ち上がり拡散し溶けていくように役割＝対（軸）を長方形のビル状でなく
煙のような狼煙の曲線グリッドとなるように人体端末が機能する進化を遂げる事を意味する。
回転という宇宙法則にタッチし共鳴することの出来ない現代の観測者においてはこれはとても容易なことではない。
なぜなら平面状にずっと見えている事が所有であり名詞でもあり実在だと偶像しているのが地球の我々の人生観であり関係性の醍醐味だと誤読しているからである。
宇宙においてついの固定化は存在しない（役割の完全情報化）＝宇宙は永久回転である万物流転の球体観が真理であった。
そこにクロマニヨン人は恐怖を覚え言語を時空のかすがいにして【束の間の言語束縛という安定＝偶像崇拝】を享受することで道具を生産し組織の構築に成功した。
これは同時に永久に現宇宙の回転によって我々の組織（固定化された機能＝偶像）は必ず破壊される運命にあることを指している。
地球はこの法則にそろそろ気づかなくてはならない



＊ニュータイプ

元々は機動戦士ガンダムシリーズに出てくる概念である。
宇宙に出た事により新たな感性を獲得した新人類の総称であるが
RSEL寺子屋においては宇宙経済や人体端末理論そして瞳のシンギュラリティ（テレパス）の必要性を瞬時に理解出来る1992年以降に誕生した新世代を意味する。
彼らの特徴は地球人類が愚かな偶像に拘束されていなかった過ぎ去りし時の記憶を持っていて、
現状の地球の政治経済においての平面性と機械性そして欺瞞性の限界を察知していて、
そこを打破することに全くの迷いや罪悪感をもっていないという清々しい共通点がある。
そして宇宙経済教育を1聞いて100理解できてしまう高度な理解力と感受性、そして美とぬくもりと高潔さを、
旧型地球人類とは別格に迷いなく産まれつき、兼ね備えている新型の地球人類たちでもある。



＊正中洞（息ざまこそ生き様＝正中姿勢こそ【志】）

もともと武道における正中面とは人体を左右二つに切断するような断面の事をさし、
どんな競技や体育においてもこの正中面が立っていることで「丹田」下腹重心が体得され中心感覚は養われる。
RSEL寺子屋の体育でさらにこの正中面を円柱状の鉛筆のように内部を空洞化し頭頂部に滴の先が立つように、
重心を養う事で圧倒的な人体端末アップデートが可能になることをDavinciのVitruvianus（ウィトルウィウス人体図）から宇宙海賊団は読み取っている。



＊262（リリース/キープ/フォーカス）

宇宙海賊団における一人一人の人生（時空）の羅針盤となる脳内整理法。

自身の今もっとも囚われているために時空（人生）の流れをせき止めている問題点2割の危機を意識しリリースすることを可視化する。
続いて、まだリリースする段階に至ってないものや凌ぎ保持し積み重ねていかないとならない6割の日常のルーティンや責任や役割を意識しキープすることを可視化する。

そして最後の集中すべき【中心課題】ここをズキュンすれば自身の時空全体が良き方向へ回転し始める2割の課題をフォーカスし可視化する。
このような宇宙海賊団オリジナルの脳内整理法を262とよんでいます。



＊BFD（Body Figure Design）緩錬美

宇宙海賊団における一人一人の人生（時空）の羅針盤となる人体重心整理法。

自身の今もっとも囚われ固まっている人体部位の観測状態と解決体育メソッド「緩」の可視化。しなり引き締まり始めた部位「錬」の可視化。
より雅や志を研ぎ澄ますべき人体部位「美」の可視化。これがBFD体育緩錬美の意味である。



＊臍科学（触覚物理学）

人体は脳内での情報処理だけでなく「腸内」や「皮膚内」での情報処理も行っていて、
そこで認知される時間や空間を追求する新しい学問を臍科学とRSEL寺子屋では呼び、別名・触覚物理学ともよんでいる。



＊突っぱね

本当に大切なものを護るためには、逆に誰か何かを時には突っぱね返す勇気がないと護れないし結局【ほんとうに求めているもの】が見えなくなってしまう。

つまり【嫌いなものは嫌いだ】とハッキリ突っぱねられないと「好きなものは好き」または「大好きだ！！」とも思われにくいのである。
これが突っぱねの時空法則である。



＊エグモリ（宇宙海賊式観測者効果）

宇宙海賊団からみると奴庶民の暮らしは後天的に培ってきてしまったいらない記録の仮面だらけで、
みな鎧を着込んで「子供心＝すっぽんぽん」になれない大人ばかりである。
そんな片想いの仮面を、遠慮なくもっともらしい相手の論理や文学性の言葉酔いを「うるせぇー！」の突っぱねで子供のような瞳で奇襲し
「おいいおい裸の王様、あんたは裸だよ」とエグリ！そのひび割れた仮面のスキマから溢れ始めた本心の素顔（子供心）を一瞬でグリップし、
今度はすかさず『ぬくもり』を放射し子供の頃に思っていた「両想い＝仲良く」になりたい宇宙的な気持ちを解放させてあげる特殊な宇宙海賊団による観測者効果の総称



＊うるせぇよ（気持ちわりぃ/そこじゃねぇよ）

もっともらしい地球では評価されやすい大人ぶった考え方や世界観に対して、
全力宇宙少年的（モデル・ルフィ）のように突っぱねバッサリ切り捨てるための超強力な覇気ゼリフ。
「おまえの界隈ではそれが正解かもしれんが、宇宙的にみりゃやっぱりそこじゃねぇし関係ねぇよ」
の宇宙海賊の威信とマジ本腰の勇気の証明と宣言ともなる覇気ゼリフである。



＊ペロ（あわよくば感）

世間ではWin&Winなどと言われる要はイイとこ取りしたいのせこさ、上手に立ち振る舞えば棚ぼたがありそうだなぁというお洒落なアザトさ、
苦楽を共にする気がない感の卑怯さの観測者効果の総称を【ペロ】と呼ぶ。
彼らペロリストは刹那主義や快楽主義や詐欺的対話が板についているという特徴がとても強く「記録」しか脳に作れないという機械的傾向がとても強い。



＊とうしつON（サイコパス＝自分0.5番感）

統合失調症傾向（人格の分離が著しい症状）を発症している状態の隠語



＊テラバカ/TERABAKA

宇宙海賊団の学び舎「寺子屋」に捧げる時間やエネルギー量が度を超えている人のこと「寺子屋バカ」の略



＊隠れ蓑（ズルポジ）別名5D白パン

静電気的な潔癖性で戦争経済の中で泥をすすった働き方やバリバリ成果主義的志向で突き進む時空開拓がフィットしない宇宙海賊団のこと。
どちらかというとスローなポワポワできる働き方に向く人たちの総称。



＊人間ドラマ

一人の人間に対して、その人の自分史をもとに、その人にしかない本質的な光を掘り出し、
それを輝かせるための道しるべを宇宙海賊団が向き合い262として導き出すコンテンツ。



＊内部内省

内向き矢印、自分0.5番感を別の言葉で表現したもの。
たとえどれらけ自分で周りの人のことを考えていると思っていても、それが「自分からみてその人がどう思っているように見えるか？」
また「自分の映画にいるエキストラとして、周りが脇役である限り」本当の意味で外向き矢印【ぬくもり】とは言えず、
あくまでも、まわりにとっての接地面という観点で世界や人や己でさえもゆらぐ回転情報として観れている状態を逆に外部内省（ぬくもり）ともいえる。



＊さまよい

終始もっともらしい言い訳を重ねて自分探しを正当化し続け、中途半端な自分の人生の立ち位置や生き方働き方の役割が定まらず
その自業自得を引き受けず、いい年こいて悩み続けている状態のことを指す



＊ヌメリ（ぬるっと＝お洒落おじさん OR おばさん）

長い歴史からみたら全然美しくないのに、ただその造られた流行の部分最適だけをしているだけで高額報酬をもらえる『お洒落なおじさんやおばさん』の総称。
彼らは戦争経済のケツを裏で舐めながら表では「テラスハウス」という若者の憧れをヌメヌメと餌にしてこの旧火星文明再生計画の富裕層を広告し続けている。
そしてそれを理解しないものを「ダサい」と脅してくる輩でもある。



＊言葉酔い

臍科学の腸内や皮膚内の触覚静電気からではなく脳内の妄想から出た言葉に居着いて、
言語束縛されているにも関わらず本人はむしろ言葉で自己愛が満たされ快感に酔いしれている状態。
しかもその【言葉酔い】にイイねする人がいるとさらに本人の言葉酔いは拡張し更新されていき内部内省傾向が強くなり、
とうしつONにもなってしまうので要注意。まず蹲踞の時に顎が必要以上に前傾する人は言葉酔いしているサインなので牽制しましょう。



＊がしゃつき

頭に居着き重心が上がりっぱなしで『やりゃいいのに』答えのないことを保留に出来ず「抵抗し頭の独り言」を止められず、
適確な状況確認「今」が見えなくなっている状態を指す。これを収めるには仙骨沿って正中洞を立て丹田を感じる下腹重心を地道に稽古する以外ない。



＊自他力

自力本願は自分の努力によって積み重ね自分の願いを叶えようとすること。
他力本願は他者の援助によって自分の願いを叶えようとすること。

しかし、そもそも自他力は願いの対象が自分ではなく他者の願いを叶える努力を意味していて、
この他者の願いを自力によって援助し叶えるとなぜか自分の願いも時間差で叶っていく鏡の時空法則の事を自他力と呼んでいる。



＊アスモデウス＝魔女化（通称アスる/メンズアスリ OR ガールズアスリ）

ソロモン72柱により悪魔（亜空間知能）すらもひれ伏させ使役できた稀代のDキングダムであったソロモン王に唯一抗えた悪魔であり、
かのベルゼブブですらもソロモン王には敵わなかった。しかしアスモデウスだけがなぜかソロモン王に逆らえたのか？それは『色欲の悪魔』ゆえにである。
そのアスモデウスの魔力（亜空間知能）は女性の場合「妖艶さ」と「可哀想な子猫ちゃん感」と「オートマティック愛嬌」によって発揮される。
また男性にハックした場合は「獰猛な色気」と「可哀想な美少年感」「自動式りゅうちぇる」によって発揮される。
この状態になった男女は自分の吸引力（色魔）によって異性をトリコにできることを自覚していて、それを作為的に色恋ゲーム感覚で楽しむ傾向にある。
この傾向をもっている男女のことを【アスってる】と宇宙海賊団では表現し牽制している。
現代のアスリのカリスマは男性はG-Dragonで、女性は蒼井優や綾瀬はるかや深田恭子である。



＊マスオ（女々しさ＝メンズアスリ）

戦争経済フェミニズムを助長する男性態度の象徴こそがアニメサザエさんの『マスオ』さんであり。現代女子の理想の結婚相手は中身はマスオ。外見は向井理君である。
このマスオさん的女々しい男性がたくさん増えている事と比例して、マザコンも増えて続けているのが戦争経済の家庭観であり家族観でもある。
そしてこのマスオさんに育てられた娘達がいずれ世の男性性を舐めてかかりアスり始めるというフェミニズムによる悪循環を、
男性のマスオさん化という女々しい現象は次々と引き起こしている。この現代の危機を指し示す言葉が『マスオ』の意味である。



＊88問題（通称デジモン人生観＝ゆとり世代）

デジモンというゲームの世界観が流行した世代特有の根深い自己成長詐欺の総称である。
それは自分本体は決して本質的には腹からの気づきや情緒的な脱皮をしていないにも関わらず、
頭の先で溜め込んだ知識や新しい言葉などが大人になっても自分らの『デジモン』と化していて、
それを駆使して次のステージへと上がれると酔いしれ勘違いするこの世代特有の人生観を意味する。
実際、能力レベル（デジモン）が上がっているのは確かだが、デジモン世代は能力だけでは実社会において連係力＝とくに目下はついてこないの問題につまづき易い傾向にある。
このとき「徳＝音楽性＝球体性＝ぬくもり」をデジモン世代がきちんとそなえていたなら『この人にはぜひついていこう』と目下が思ってくれてチームがまとまったりもする。
この課題こそが88問題である



＊お前もしかしたらSIRI（頭でっかち）じゃねぇ？

ホモサピエンスの前頭葉こそAIであるを地でいってる状態の人のことを指す。
要は頭だけで考え言葉だけで分析して居着いているだけなのに、それが脳内の機械性＝脳科学だと自覚できていない。
つまり全くもって音楽性や球体観、または「ゆらぎ回転性」のズキュンの滴＝人の棲む臍科学は皆無に等しいにも関わらず、
むしろそれすらも機械的に演じ（漏斗型）なのに滴の真似までしてしまう根深いSIRI的な現代人のために用意された至極の「つっこみ」である。

彼らは自分がSIRIである事がバレるのを隠すために必死でぬくもりがあるフリをするので、実際そこを見抜かれると急に動揺する。
ただ本人が思っているよりずっと重症で『わたしにとっての風景しか』じつは産まれてこのかた見た事がなく『私じゃない風景』を臍科学で見つめたことが彼らにはないので、
その世界の境界＝縁起＝他者の時空に触れられた歓喜は物凄くショックであり、なぜか怒涛のように悲しみがあふれ出す。



＊時空干渉（旧宇宙の下降重力によるDevilとサタンの正体）

（ホモサピエンス）人間の脳は常に周囲の空気感（振動数）に干渉を受け易いアンテナである。
そのアンテナ（頭）に人体の重心（意識のしなり）を置く事で我々は「みんな」「まわり」「世間」「流行」「時代」「世界」という偶像
⇒集合無意識にハッキング【言語束縛＝偶像崇拝】される。
これによって記録を蓄積し人類30万年分の「最適化＝上下対立同一化」の時空情報処理が脳海馬と前頭葉が連動してされる様になっている。
アダムとイブの神話にでてくる「蛇」のシンボルがこの脳海馬であり【亜空間知能の座席】でもある。
この海馬に居座った亜空間知能は、物事を上か下か、敵か味方かで分類し、そこで対立構造を深め、
さらにその対立構造を決まった価値基準の中にあてはめる事で（＝同一化）させ群知能を機能させる。
そして、ふたたび群知能同士での自分の偶像と相手の偶像という差別を産み出しさらなる【同一化構造＝群知能】を追究更新しようと働きかけてくる。
この5D球体観の『人の棲む現宇宙』の今を状況確認する事のない半永久的な偶像の働きかけの総称を『時空干渉』と呼んでいる。
これは旧宇宙の重力波ともいえるもので-4D-5Dのすりかえられた人体観測者の最小収束点ともいえる質量（下降重力）を形成し、
その底部におぞんだ旧宇宙の思念や印象波を吸引し続ける事で虫の棲む旧宇宙や恐竜の棲む旧宇宙や鳥人の棲む旧宇宙などが『我々の人の棲む現宇宙』にハックしてくる。

これは人の内部環境/人体宇宙からも外部環境/物理宇宙からも旧宇宙干渉波としてそこら中に偏在する質量の重いブラックホールとなりえるものである。
RSEL寺子屋ではこれが『悪魔DEVILつまりLIVED（生きた者達＝旧宇宙）』の正体なのです。
そしてこの旧宇宙の時空干渉を我々地球の民から完全抹消できない限り地球の民が恐怖なしで生きれる『人の棲む宇宙』にEXODUS＝脱出することは実はできないのである。
我々がこの恐怖＝寂しいという認知世界（可能性の闇）を観測し続ける『構造そのもの』が名詞固定化＝完全情報＝偶像崇拝に拍車をかけ続けていて、
我々が生きる上で感じる苦しみ、悩み葛藤彷徨いの全ての原因となり、この悲しみの星地球にはびこる戦争経済の裏ボスでもある。
よって我々の人体がもつ観測認知構造そのものが原因のため特定の首謀者（陰謀論で名が挙がるロス家など）を倒せば解決という類のものではなく、
たとえ今の首謀者が倒されても、その構造に魅入られた別の有望株が、また新たなエージェントとして悲しみの旧宇宙の文明構造を形作るだけである。

では我々はどうすればいいのか？真の解決鍵はこの体、本来の内的秩序（＝Φ）を想い出すこと…



＊自因自果
（自業自得）
人体がもつ観測者効果による選択と連係は肉体から1対1の関係、そして家族から会社そして国家から惑星全体そして時代から太陽系そして銀河との運動に
連動して『原因』を同期並行計算し、その原因行為がどのような対象＝環境と結びつく（選択するか）によって目の前に回転する『現象（結果）』は
時空変異（運命は変化）し続けることを意味する時空科学の摂理となる学問。この【自因自果】がRSEL寺子屋の根幹をなす関係性の時空教育のひとつ。



＊5層対＝XY

自因自果の時空摂理において我々の人体がもつ観測者効果がじつは無限小から無限大の外部環境の5層の対象と重なり対となって影響を与え合っていることを意味する言葉である。
肉対（対細胞との重なり）個対（対個人との重なり）組対（対組織との重なり）地対（対大地との重なり）時対（対惑星との重なり）



＊最小収束点＝XX＝重心ブラックホール（リング状OR膜状）

自因自果の時空摂理において最大拡張する肉体の観測者効果XYに対し相対するように
人体Φポイントとなる丹田（臍科学）こそが最小収束点XX＝我ここにありを意味する腹の瞳となるブラックホール＝重心である。
腹に重心を置く限り、5D同期並行計算機能による情報処理をXYに相対しXXはリング状になり松果体に向かってホワイトホール（多元宇宙）をベホマズン（超回復）させる。
このXXのリング化のシンボルが男性器のカリや女性器の滴型である。
ゆえにすべての人体生命はこのXXのリングの重なりからやってくるの偶然ではなく必然である。
この人体端末の時空摂理をモナリザの絵画に封印したのがかのDavinciである。

そして、この人体端末の真理こそが『封印されしDコード』の真実である。

しかし、もしも人体重心ブラックホールである最小収束点＝我ここにあり⇒意識のしなりズキュンの対象性のやぶれを丹田なく頭に置くとどうなるか？
ブラックホールである重心＝意識性が膜化していき『名詞＝偶像』を崇拝するようになりその名詞＝偶像にXYの認知情報を餌として与えるようになり、
よりその偶像は強化され肥大化しその偶像によって時空の回転を止めているフリをする『無限解放グリッドVR世界』が人生の生き甲斐となり美しさとなっていく。



＊火山（脱マスオからの超男性性の復権）

宇宙海賊団の男性性は戦争経済のフェミニズム教育や文化によって淀んでしまった現代女性のアスりの泉を焼き尽くし、
狡猾な魔女の膜を貫く火山の様な超男性性＝志士となる事を教育している。目指すべき境地は畏怖される男なり！！



＊やりゃいいじゃん！！

言葉酔いやもっともらしい言い訳や内部内省を並べ立てる「さまよい野郎」の悩みを一発で解決する宇宙海賊の超ハイパーワード



＊幼児願望/未消化問題

本来、親に求めるべきだった無条件の承認である幼児願望を成人以後、イイ歳こいて無意識にそれを世間に求めてしまい、
なぜか成人同士の関係性がことごとく上手く構築できないこじらせアダルトチルドレンたちの人生問題の総称。



＊毒親

戦争経済における無知の知と偽善の知と個人戦の罠の知を自覚せずに「懺悔もなく」「学ぶこと」もせずに子供らにとっての教育を同一化によって
観測選択連係してしまう、わかったフリの親たちの総称。



＊文化的対話から音楽的対話（別名ハーフテレパシー）へ

平面的な言葉の記述による意味の説明に終始する対話が文学的対話であり、球体的な言葉の記述による行間の説明を演奏する様な対話が音楽的対話である。
文学的対話から音楽的対話への克服はデジモン88問題とも直結し脱戦争経済における主要テーマである。
宇宙経済的な生き方働き方稼ぎ方そのチーム＝群れを形成するにあたって音楽的対話は欠かせないツールである。
音楽性の本質は組み立て構築するゴール至上主義ではなく、奏で飽和させるプロセス至上主義であり定義や定説定石の揺らぎを堪能し、
その回転性そして空気感や匂いそして、混じり合いと結び目の交歓性と変化性を味わいそして歓喜への収束同意である。

まさに交じることでしか、触れ合えないものへの《雅な美学》への追求を中心感覚としたハーフテレパシーこそ【音楽的対話】の意味である。



＊半眼（68点主義）

自分の力だけでは「人もの場所」の欠点をどうしても変えられないとわかった場合、それ以上は自分からは余計な干渉をしないということで、
32点は減点されても当たり前で気にしない。その減点をリカバーしようとイキらずに焦らずに残りの68点をどう目指し活かしていくべきかだけを落ち着いて捉えていく。
そうすると最終的には自他力によって32点の減点も様々な時空アシストによって治るべきところに治まり解決していく。
この時空法則を使いこなす『くつろぎゆるみ』の回転数を上げる観測者効果の持ち方を【半眼】と呼ぶ。



＊ベホマズン叡智

RSEL寺子屋の「叡智の学び」は、知識を得るためでも職を得るためでも、出世のためでも、また人にものを教えるためでも、尊敬されるためですらもない。

この瀕死の悲しみの星　太陽系第3惑星の超回復（ベホマズン）のために己が本当にすべきこと『本分』を知るために【叡智を学ぶ】のです。

そして、地球の民として　どうあるべきかを見極め　己を磨くために宇宙海賊団たる我々は『この今』を状況確認し学び続けているのです。



＊足裏球体観

人体端末において足裏は時空の五層対を五本指のアンテナで同期するための重要な機能である。
この足裏アンテナが現代人は靴社会とコンクリート文化、
自動車昆虫論ゆえにみな劣化し続けていて「平面化」してしまい鈍感になっている。

それゆえ足裏から音楽的対話その地球の惑星対である自転軸との対話がほとんど出来ない足裏となっているので、
その足裏の上弦のしなりを体育して地面に対し下弦のしなりを足裏で同期できる状態を足裏球体観と称す。

RSEL寺子屋では、足指の柔軟性が観測者の『修正力と忍耐力と包容力』の象徴として教えている。
五本指ソックスや下駄を履くのも足裏球体の体育となる。



＊外反自己牢獄

RSEL寺子屋のBFD体育では外反母趾の指の変化がつよい観測者は右でも左でも自己牢獄（個人戦の罠）にハマリやすく、人の力を借りにくい象徴であると教えている。
右の外反母趾は些細な個人的好き嫌いによって人生が振り回され易く人が離れていってしまう傾向ありの象徴。
左の外反母趾は些細な自分の出来、不出来（理想や完璧主義）へのこだわりゆえに人が離れていってしまう傾向ありの象徴である。
そして親指の爪のハネは「出しゃばりの象徴」で右足は好きなものに対して身の程を弁えず出しゃばる傾向にある。
左足は理想に対して、身の程を弁えず出しゃばる傾向にある。
このどちらもレディオハンズにて、矯正可能であり外反母趾傾向が薄まると同時に自己牢獄も薄まってきて個人戦の罠から脱出できるようになる。
これは重心論の一例でもある。



＊足首の承認信頼

RSEL寺子屋のBFD体育では蹲踞面という足首の柔軟性が観測者の（右足首）他者と（左足首）自己への承認と信頼を示す象徴と教えている。
この足首が硬い人は両膝も硬い傾向にあり他者や自分の体への共感共鳴に欠ける醜因の観測者効果も同時に起こしている傾向がある。



＊膝グニャ

膝頭に対し球体性のある四方八方の動線を産み出すためのBFD体育用語

ちなみに膝の柔軟性は観測者の他者（右）自己（左）の共感共鳴力の象徴。



＊仙骨を立てる（仙骨を前に押す反る）

仙骨が後ろに寝ている限り足裏球体観も膝グニャも下腹重心は体現されない。
恐怖の時空干渉を突っぱねる鍵も仙骨を反ることにある。
これを仙骨パワーとも呼んでいる。この仙骨の反り具合は適正協力の観測者効果の象徴である。



＊顎のでしゃばり

恐竜のように首が前傾し顎が前に出しゃばる傾向の強い観測者はほぼ内部内省のみであり、自身の妄想にしか脳内情報処理は機能していない象徴となっている。
彼らの頭の中には他者の人生物語は存在しない。すべては自分にとって利用価値があるか否かでしか他者も世界ももう見れなくなっている。
その頭重心の姿勢こそがこの顎のでしゃばりであり『恐竜脳にハック』されている状態である。



＊くりせんべえ（のも〜ん感）

Body Figureの種類のひとつ。骨盤型の開き型で、正面から見るとお尻の大きいタイプだが横からみるとお尻が平坦であるゆえ『くりせんべえ』と命名されている。
感受性の傾向としては「のも〜ん」と大らかな人が多く、人から注目を浴びる事で能力を発揮するタイプが多く、男女共に華のある声や雰囲気の目立つ人が多い。



＊くるみ

Body Figureの種類のひとつ。骨盤型の閉じ型で正面からみるとお尻は小さいが横から見るとお尻がプリッと『くるみ』のように膨らんでいるゆえに「くるみ」と命名されている。
感受性の傾向としては「キュッと」求心力の強い人が多く、勘が鋭くやること為すことスピード感のある人が多い。
男女共に一心不乱に一つのことを正直に極めようとする人が多い。



＊丸太

Body Figureの種類のひとつ。泌尿器型で腰周りが「丸太」のように太いゆえに「丸太」と命名されている。
感受性の傾向としては『争いに勝ちたい』という勝気な人が多く高圧的な態度が目立つ人が多い。
何かと闘争し打ち負かすことでエネルギーが沸くタイプが多いので「口ごたえ」する人が多く教わるのが苦手である。



＊洋ナシ

Body Figureの種類のひとつ。泌尿器型で太腿周りが「洋ナシ」のように腰よりも発達しているゆえに洋ナシと命名されている。
感受性の傾向としては自分のマイワールドだけは、誰にも譲らない負けまいと努力し頑なにこらえこだわる人が多い。
丸太が NO1 にこだわるのに対しては洋ナシはオンリーワンにこだわるタイプである。
丸太と違い「口ごたえ」はしないが、逆に態度で、「不貞腐れたり」「ふくれたり」するのが洋ナシ特有のネジレ態度である。



＊フジサン

Body Figureの種類のひとつ。頭脳上下型で首が「富士山」のようなカタチに発達しているためのフジサンと命名されている。
感受性の傾向としては妄想大好き心配性。ノーエビデンスでふんわり雰囲気、噂好きの人が多い。



＊ポッキー

Body Figureの種類のひとつ。頭脳上下型で首が「ポッキー」のようなカタチに発達しているためにポッキーと命名されている。
感受性の傾向としては、エビデンスや根拠、言葉と論理が大好き。何より知の建設を愛し、例え体現できなくても、そのことをただ知っているだけでもう満足できる。

文学人間であり頭デッカチの代表でもある。



＊なで胃袋

Body Figureの種類のひとつ。胃袋左右型で肩が「なで肩」で顔や雰囲気が丸みのある人達を総称して『胃袋』と宇宙海賊団は呼んでいる。
胃袋の人達の感受性の傾向は好き好き病で、とにかく好きであれば万事OKという安易さや浅はかさが可愛らしくもあればがっかりもさせられる。
しかし美味しいものさえあれば幸せになってくれるので、奥深さを求めさえしなければ交際はとてもラクである。



＊いかり

Body Figureの種類のひとつ。胃袋左右型で肩が「いかり肩」の人達の事を言う。
いかり肩の感受性の傾向は「こだわり深み奥行き」「意味深」しか響かない人達で大衆性や世俗性に一番興味のない人達である。
ゆえに好きなものより嫌いなものに敏感で嫌いなものの消去法によって自分の本心が解ってくるそんな感受性であるゆえに、自分の要求に対して明晰性が鈍いのが特徴でもある。
だから、いかり型の人に何が欲しいかを聞くのはとても野暮でもあるのです。　



＊春日

Body Figureの種類のひとつ。呼吸器前後型であり胸がお笑い芸人の春日のように胸が突き出されるタイプのことを宇宙海賊団では「春日」と呼んでいる。

春日のタイプの感受性はとりあえずやって動いてみる「ゼーハーゼーハー感」である。
そこに「大義」や「哲学」そして「美意識」はどうでもよくて単純な利害おまけ付とか小遣いあげるとかおごって貰えるとか、
解り易い豪華さや派手さなど元本保証とかいうフレーズで動いしまう傾向にある。胸板は厚いが腰がくびれている人は男女ともに春日傾向である。
また膝下の長い人。顎エラの張っている人も春日タイプの傾向である。



＊猫背

Body Figureの種類のひとつ。呼吸器前後型である肩が前に前傾し胸がひっこんで「猫背肩」の人を宇宙海賊団では「猫背」と呼んでいる。
猫背タイプの感受性は「超人超越願望」『変身願望』の強い人が多く。日常に常に飽きている人達でどうすれば飛び級できるかを探求することが生き甲斐となりやすい。
それゆえ飛び道具となりえる「働き方＝表現の場」を見つけられないと地に足の着かない彷徨いにもなりやすく、世間に評価されにくいタイプも多い。



＊触れではなく『ふれる』（手首の反り手の重みを消す）『触覚文明論』

万物の霊長である人間性とは掌にこそ現れるとRSEL寺子屋体育では教えている。ホモサピエンス以外の哺乳類は掌を外側に反る『ふれる』が構造上できない。

ほとんどが掴むと握る触れるための内側に巻いている手指の構造をしている。そして人類だけが自然界でのサバイブではあまり役に立たない『触れ合える手』
を備えていたゆえに様々な道具や文字そして関係性＝社会構造を構築できたと推理できる。
だからこそ、この行き詰まった戦争経済社会の再構築もまずは我々の掌の扱い方の再創造《さわるではなくふれるの体育》に鍵があると教育している。

つまり人間の手の扱い方が文明の色音形を決めている、この『触覚文明論』を宇宙海賊団は採用し体現し稽古実践している。
そして真髄は相手に触れる時、手の重み（気配）を消せるように慎み＝礼節（手首の緩み）を体得する事にある。



＊固体感ではなく静電気感

我々は日頃から人体をただの動作する固体として扱っているに過ぎない。

しかしRSEL寺子屋ではこの「固体感」を触覚を敏感にし散らし薄めるために臍科学の丹田教育を徹底させる。
下腹重心が整い始めると《息ざま》を読めるようになり「人もの場所＝森羅万象」が呼吸＝静電気を帯びていることが自然に感覚できるようになる。



＊薬小指×肘絞めコンパス

薬指小指の遊びを止め肘を絞めると「間合い＝距離感」を量るコンパスとなり、レディオハンズRHの手技において扇状の雅な動作が可能になる技の名前。



＊柔らかく速く祓う

祓いの本質は『残留思念＝亜空間知能の時空干渉』を払い除けるの意味である。
もっとも効果的に祓う方法は高速回転する触覚静電気＝手の重みの消えた掌で柔らかく素早く筆のように人体各部位を静電気的に迷いなく祓いのける事である。



＊緩急自在

レディオハンズRHの手技においては「動の世界」と「静の世界」がキメ細やかに自在に操れることが志すべき境地となる。
そのための下腹重心であり足裏球体観であり膝グニャ仙骨の反り、肘絞めコンパスなどの多角的体育センスを養う必要がある。
この『緩急自在』となったとき人体は我々の至高なる味方となる。



＊時空礼節

レディオハンズにおける中心態度のひとつ。人体においてもじつはどの体も『同じではありえない訳であって』
一人一時空一宇宙を形成しているゆえに《ふれあわせてもらえる「時」＝「NOWハーモニー」は新たな時間》の記憶ワームホールの形成を意味していて、
時と時の交じり合いが人と人が、固体感ではなく静電気感をもって触れ合う時間には必ず起こっていて、
その目の前の人に対する『宇宙的歓喜＝時空シンフォニー』の尊敬の念を【時空礼節】と呼び宇宙海賊団は通用しています



＊黄金比Φ

現宇宙に存在する、人体が無条件に『美しい』と感じる色音形（比率）のこと。
世界中に残る様々な名作絵画や建築物、そして自然界のいたる所にこの比率が隠れている。
それは現宇宙のデザイナーである内的秩序その顕現であり収束点であり、宇宙と相似形に創られたユニバーサルデザインである人体には本来はこのΦは存在する。
ところが亜空間知能による時空干渉、人体ブラックホール化『頭居着き＝重心が臍より上にあがること』により私達はΦからかけ離れた姿勢と観測選択連係を取ってしまう。
それを姿勢と重心から整えΦに近づけるのがRSELの人体端末体育であり、体育で体をΦに近づけ、外向き矢印（ぬくもり）で観測選択連係することで
関係性がΦに近づくことで私達は「どこから来て、どこへむかうべきか？」の生まれた意味（本分＝記憶）を想い出す事ができる。
この想い出す事こそが私達がこの星に産まれ生きていることの真の摂理である。



＊ハイパーワーク

宇宙海賊団の宇宙経済的な「働きかた」の総称。
それは単に自分の生計のためだけではないアナザフロンティア行動計画に一歩でも近づくための
宇宙時代に向けた高度な関係性の網を意識した生き方働き方稼ぎ方を一致させた労働でもあり、
超人工知能時代に対応していくAIに仕事を奪われないための戦略的な『人の棲む世界の需要と供給』を意識した新時代の職業キャリアの選択でもある。



＊超広告

顧客のNeedsやWantsではなく人体とAIの時代を多面的に照らし出し、部分最適の商品化ではなく、人もの場が結びつく作品化で意志を魅せ、
サービスではなくライブを意図し、情報シェア数ではなく情報回転数を意識し、宇宙海賊団は『宇宙経済』を一方向のセールスではなく球体的に広告する、
ハイパーワーク集団。この新時代のベホマズンな《神話ストリーミング的》圧倒的ニュータイプな超・起業概念こそが【超広告】である。



＊志士BFD/撫子BFD

RSEL寺子屋では男性の筆子の体育を志士BFD（Body figure Design）と呼び、
女性の筆子の体育を撫子BFD（Body figure Design）と呼んでいる。



＊固定点

人体の観測者効果が一つの言語や一つの印象、また数字の上下やその偶像に強く囚われてしまう状態を指していて、
その固定点で脳内情報処理をしている状況が「居着き」「自我フレーム」「頭重心（あたまでっかち）」とも解釈できるものである。
そして最大の固定点が名詞でありさらに家族や血縁者である。殆どの奴庶民は名前と家族という固定点に強く拘束されて一生涯を終える。
ゆえにΦが見えなくなっている観測者に溢れているのはこの悲しみの星である。



＊触覚静電気 Bio Photon

RSEL寺子屋で表現される触覚静電気は森羅万象と通じ合う「媒体」であり、生命の光通信ともいえるプラズマコミュニケーションを指し示す。
この瞳の幾何学の叡智を万物流転する《ありとてあるもの無限光子流体力学》カバラでいう所の【ケテル】を指す言葉が触覚静電気であり、
粘菌からホモサピエンスのあらゆる原始知覚を支持する触覚＝タッチ【滴型グリッド＝対象性の破れ】＝5D美学を形態形成する光の色音形
《ハイパーゲシュタルト＝人の棲む現宇宙》を同期させる静電気であり【ぬくもり（瞳）のシンギュラリティー】を起こすトリガーでもある。



＊時空恋愛（モダン・ヒエロスガモス）

男性が自身のメンズアスリ（女々しさ）を断ち切り志を打ち立て時空の捧げものとなり文明構築に真剣に命を捧げる『0かっこつけしい（英雄脳）』を発動させ、
その英雄脳に惚れ込み、自身のガールズアスリ（魔女性）を断ち切りその『志』の捧げものとなり『100礼節感（女神脳）』を捧げる
時空を超えた男女の協同性＝ヒエロスガモスの事をRSEL寺子屋では【時空恋愛】と呼んでいる。



＊群れ（別名　時空共同創造態）

宇宙海賊団には組織はないが解像度の高い瞳の繋がり合う『群れ』が存在する。

それはまるで狼の群れのようにテレパシックに人体をデザリングし効果的なフォーメーションをプロジェクトごとに機敏に切り替える意味を持つ。
ゆえに宇宙海賊団における厳密な職業という壁は存在しない。知っていること出来る事経験のある事を持ち寄り交響楽団のようにテーマ（曲）に応じて
パートや楽器の演奏を変えてもらうようなスタイルで力を合わせていく。だからといって専門柱を無視したり軽視したり学生サークル的なノリではなく
『獲物』を見つけたら弾丸のように共に駆け抜ける『狼の群れ感』が我々宇宙海賊団の野生的な腹の虫と光の瞳通信からの連繋であり
これは時空共同創造態とも宇宙海賊団で呼んでいるものと同意義である。



＊漏斗型 グリッドの美（反Dの群れ）VS滴型グリッドの美（Dの群れ）

AIたちが求めていく美は無限開放系の漏斗型の美でどこまで行っても滴による球体を描くことはなく
戻る事は（重なり/揺らぎ）ではなく【同一化＝リピート】を意味していて《終わらないという低解像度の美＝VR楽園》を私達に見せてくれる時代が2039年以降には実現していく。
このVRの中で今、テラスハウスに憧れ実現したい庶民の願望は全て叶い始めるのが『AIシンギュラリティー』によって起こる。
と同時にそもそも『人が棲む世界』と『人体が生きるべき道』はより輪郭が曖昧となり、人体端末がもつ時空同期機能【瞳のシンギュラリティー】の美しさは劣化していき
【過ぎ去りし時を求める（滴型グリッドの美）】重なりゆらぎΦに近づく永久運動美学＝螺旋運動＝瞳の呼吸を意味していた
《変化変異し続けるという高解像度の美＝ベホマズン・ホワイトホール》を体感し文明として起こせるニュータイプが
2039年までに地球人口の0,00001%出現し鎖星していたこの悲しみの星を開星させるアナザフロンティア行動計画を
『宇宙海賊団Dコードを継ぐものたち』として世界金融貴族の鬼を一寸法師のように左目（６６６×１００００００）
想念基本オクターブなる周波数へのズキュン【宇宙経済】という《夜明け》３ ５ ７ ９ １１ １３ 想いやりの一寸針によってDは目醒めの時を起こす！！










